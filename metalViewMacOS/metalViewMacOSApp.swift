//
//  metalViewMacOSApp.swift
//  metalViewMacOS
//
//  Created by Raul on 1/8/24.
//

import SwiftUI

@main
struct metalViewMacOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
